import copy
import io
import logging

from datetime import datetime
from lxml.etree import parse

logger = logging.getLogger(__name__)

NS = {"svg": "http://www.w3.org/2000/svg"}
INKSCAPE_LABEL = "{http://www.inkscape.org/namespaces/inkscape}label"
XLINK_HREF = "{http://www.w3.org/1999/xlink}href"


def render_datetime(dt, tz):
    return f"{dt.astimezone(tz):%Y-%m-%d %H:%M}"


def transform_svg(in_file, out_file, context=None, ignore_error=False, tz=None):
    """
    Transform in_file svg to outfile. Will walk through text-nodes and and evaluate labels and replace text with
    result. A text-node containing a tuple like (x-offset, y-offset, list) will make new text-nodes with x/y offset
    for every element.
    :param tz: timezone to use for document
    :param ignore_error: use empty-string on Exception, don't log stacktrace
    :param in_file: Filename or file
    :param out_file: Filename or file
    :param context: Dictionary with extra context
    """
    if context is None:
        context = {}
    tree = parse(in_file)
    root = tree.find("svg:g", NS)
    text_nodes = root.findall("svg:text", NS)
    for node in text_nodes:
        tspan = node.find("svg:tspan", NS)
        label = node.attrib.get(INKSCAPE_LABEL, False)
        if label:
            try:
                result = eval(label, globals(), context)
            except Exception:
                if ignore_error:
                    root.remove(node)
                else:
                    logger.exception("Template error")
                    tspan.text = "#Error"
                continue
            if isinstance(result, tuple):
                x, y, lst = result
                root.remove(node)
                node.tag = "text"
                node.find("svg:tspan", NS).tag = "tspan"
                for i, n in enumerate(lst):
                    node = copy.deepcopy(node)
                    tspan = node.find("tspan")
                    tspan.text = str(n)
                    if i != 0:
                        new_x = str(float(tspan.attrib["x"]) + x)
                        new_y = str(float(tspan.attrib["y"]) + y)
                        tspan.attrib["x"] = new_x
                        tspan.attrib["y"] = new_y
                        node.attrib["x"] = new_x
                        node.attrib["y"] = new_y
                    root.append(node)
            elif isinstance(result, datetime):
                tspan.text = render_datetime(result, tz)
            elif result is None or result is False or result == "":
                root.remove(node)
            else:
                tspan.text = str(result)

    image_nodes = root.findall("svg:image", NS)
    for node in image_nodes:
        label = node.attrib.get(INKSCAPE_LABEL, False)
        if label:
            result = eval(label, globals(), context)
            node.attrib[XLINK_HREF] = "data:image/png;base64," + result.decode("utf-8")

    tree.write(out_file)
    if isinstance(out_file, io.IOBase):
        out_file.seek(0)


def transform_svg_to_string(in_file, context=None, ignore_error=False):
    """
    Same as transform_svg but will return a string with transformed svg.
    :param ignore_error: use empty-string on Exception, don't log stacktrace
    :param in_file: Filename or file
    :param context: Dictionary with extra context
    """
    if context is None:
        context = {}
    out_file = io.BytesIO()
    transform_svg(in_file, out_file, context, ignore_error)
    out_file.seek(0)
    return out_file.read().decode("utf-8")
