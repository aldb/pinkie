import base64
import io
from datetime import datetime, timezone
from unittest import TestCase

from pinkie import transform_svg
from pinkie.svg import render_datetime


class Test(TestCase):
    def test_transform_svg(self):
        with open("result.svg", "rb") as ground_truth:
            with io.BytesIO() as result:
                transform_svg("test.svg", result, {"a": 42})
                assert ground_truth.read() == result.read()

    def test_image_transform(self):
        with open("image_result.svg", "rb") as ground_truth:
            with open("thumbsup.png", "rb") as icon:
                with io.BytesIO() as result:
                    transform_svg(
                        "image.svg", result, {"image": base64.b64encode(icon.read())}
                    )
                    assert ground_truth.read() == result.read()

    def test_render_datetime(self):
        tz = timezone.utc
        dt = datetime(2020, 6, 11, 14, 49, tzinfo=tz)
        self.assertEqual("2020-06-11 14:49", render_datetime(dt, tz))
