import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pinkie",
    version="5.0.0",
    author="Andreas Liljeqvist",
    author_email="andreas@aldb.se",
    description="Library for transforming svg by evaluating labels produced by inkscape",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/aldb/pinkie/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=["lxml"],
)
