# Pinkie

## Usage

Create an svg with inkscape.  
Place your text/pictures.  
Text-nodes containing a label will be replaced with the value of `eval` on that label.  
Furthermore you can construct lists of text by returning a tuple on the form `(x_offset, y_offset, list)`.  
Example `(20, 5, range(10))` - Will result in text-nodes from 0 to inclusive 19 with an x-offset of 20 pixels.  
Labels that cast an exception will leave the text `#Error` and log an exception.

### Python usage
```
from pinkie import transform_svg
transform_svg('template.svg', 'transformed.svg')
```
`Transform_svg` take an optional dictionary providing extra context.  
Both file-arguments support filenames or file-objects.